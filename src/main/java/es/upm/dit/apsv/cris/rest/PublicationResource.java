package es.upm.dit.apsv.cris.rest;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jsonp.JsonProcessingFeature;

import es.upm.dit.apsv.cris.dao.PublicationDAOImplementation;
import es.upm.dit.apsv.cris.model.Publication;

@Path("/Publications")
public class PublicationResource {
	
	@GET
	@Path("{id}")
	public Response updateCiteNumber(@PathParam("id") String id) {
	String APIKey = "ed8038e9ab8b448e5f42af83639b12a6";
	Publication p = PublicationDAOImplementation.getInstance().read(id);
	if (p == null)
	  return Response.status(Response.Status.NOT_FOUND).build();
	Client client = ClientBuilder.newClient(new ClientConfig());
	JsonObject o = client.register(JsonProcessingFeature.class).target(
	                "https://api.elsevier.com/content/search/scopus?query=SCOPUS-ID("
	                   + id + ")&field=citedby-count")
	                .request().accept(MediaType.APPLICATION_JSON)
	                .header("X-ELS-APIKey", APIKey)
	                .get(JsonObject.class);
	try {
		JsonArray a =((JsonArray) ((JsonObject) o.get("search-results")).get("entry"));
		JsonObject oo = a.getJsonObject(0);
		int ncites = Integer.parseInt(oo.get("citedby-count").toString().replaceAll("\"", ""));
	
		p.setCiteCount(ncites);
		PublicationDAOImplementation.getInstance().update(p);
	} catch (Exception e) {}
	
	return Response.ok().build();
	}

}
